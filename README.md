# GM-projet-1

Factorisation LU et élimination de Gauss

Installer Numpy :  
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py  
sudo python get-pip.py  
sudo apt-get install python-dev  
python -m pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose

ou

sudo apt install python3-numpy python3-matplotlib python3-scipy python3-sympy
