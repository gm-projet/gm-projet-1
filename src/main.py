#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy
from scipy.linalg import hilbert
import time


class NonSquareMatrixError(Exception):
    pass


DIM_MAX = 10


# retourne [L, U] tel que L*U = M et L matrice triangulaire inferieure et U matrice triangulaire superieure
def factoLU(M):
    A = numpy.copy(M)  # on stocke la valeur de M pour travailler dessus
    n = A.shape[0]  # on récupère la taille de la matrice
    try:
        if A.shape[0] != A.shape[1]:
            raise NonSquareMatrixError
        L = numpy.identity(n)
        try:
            for k in range(0, n-1):
                for i in range(k+1, n):
                    L[i, k] = A[i, k] / A[k, k]
                    for j in range(k+1, n):
                        A[i, j] = A[i, j] - L[i, k] * A[k, j]
                if A[k, k] == 0:
                    raise ZeroDivisionError

            U = numpy.triu(A)
            return[L, U]
        except ZeroDivisionError:
            print('Toutes les sous-matrices ne sont pas inversibles donc il n\'y a pas de factorisation LU \n')
            return[numpy.zeros([n, n]), numpy.zeros([n, n])]
    except NonSquareMatrixError:
        print('La matrice n\'est pas carrée')
        return [numpy.zeros([n, n]), numpy.zeros([n, n])]


def resoLU(M, b):
    try:
        x = numpy.zeros(b.shape)
        A = numpy.copy(M)  # on stocke la valeur de M pour travailler dessus
        v = numpy.copy(b)  # on stocke la valeur de b pour travailler dessus
        if A.shape[0] != A.shape[1]:
            raise NonSquareMatrixError
        n = A.shape[0]  # on récupère la taille de la matrice
        try:
            for k in range(0, n-1):
                for i in range(k+1, n):
                    p = A[i, k] / A[k, k]
                    v[i] = v[i] - p * v[k]
                    for j in range(k+1, n):
                        A[i, j] = A[i, j] - p * A[k, j]
            A = numpy.triu(A)
            # resolution
            for i in range(n-1, -1, -1):
                if A[i, i] == 0:
                    raise ZeroDivisionError
                x[i] = (v[i] - sum([x[j]*A[i, j] for j in range(i+1, n)])) / A[i, i]
            return x
        except ZeroDivisionError:
            print('La matrice n\'est pas inversible \n')
            return numpy.zeros(b.shape)
    except NonSquareMatrixError:
        print('La matrice n\'est pas carrée')
        return numpy.zeros(b.shape)


# profil est un tableau n*2 avec dans la colonne 0 les premiers indices de coef non nuls de M
# et en colonne 1 les derniers indices de coefs non nuls
def profilLU(M, profil, b):
    try:
        x = numpy.zeros(b.shape)
        A = numpy.copy(M)
        v = numpy.copy(b)
        if A.shape[0] != A.shape[1]:
            raise NonSquareMatrixError
        n = A.shape[0]
        try:
            for k in range(0, n-1):
                for i in range(k+1, n):
                    p = A[i, k] / A[k, k]
                    for j in range(max(k+1, profil[i][0]), profil[i][1] + 1):
                        A[i, j] = A[i, j] - p*A[k, j]
                    v[i] = v[i] - p*v[k]
            A = numpy.triu(A)
            # resolution
            for i in range(n-1, -1, -1):
                if A[i, i] == 0:
                    raise ZeroDivisionError
                x[i] = (v[i] - sum([x[j]*A[i, j] for j in range(i+1, profil[i][1] + 1)])) / A[i, i]
            return x
        except ZeroDivisionError:
            print('La matrice n\'est pas inversible \n')
            return numpy.zeros(b.shape)
    except NonSquareMatrixError:
        print('La matrice n\'est pas carrée')
        return numpy.zeros(b.shape)


# inverse une matrice triangulaire supérieure
def inverserMatriceTriangulaireSuperieure(M):
    U = numpy.copy(M)
    n = U.shape[0]
    try:
        if U.shape[0] != U.shape[1]:
            raise NonSquareMatrixError
        Uinv = numpy.zeros([n, n])
        # résolution par remontée des systèmes Ax = ei
        try:
            for k in range(0, n):
                ei = numpy.zeros(n)
                ei[k] = 1.
                for i in range(n - 1, -1, -1):
                    if U[i, i] == 0:
                        raise ZeroDivisionError
                    Uinv[i, k] = (ei[i] - sum([Uinv[j, k] * U[i, j] for j in range(i + 1, n)])) / U[i, i]
            return Uinv
        except ZeroDivisionError:
            print('La matrice n\'est pas inversible')
            return numpy.zeros([n, n])
    except NonSquareMatrixError:
        print('La matrice n\'est pas carrée')
        return numpy.zeros([n, n])


# inverse une matrice triangulaire inférieure
def inverserMatriceTriangulaireInferieure(M):
    L = numpy.copy(M)
    n = L.shape[0]
    Linv = numpy.zeros([n, n])
    try:
        if L.shape[0] != L.shape[1]:
            raise NonSquareMatrixError
        # résolution par descente des systèmes Ax = ei
        try:
            for k in range(0, n):
                ei = numpy.zeros(n)
                ei[k] = 1.
                for i in range(0, n):
                    if L[i, i] == 0:
                        raise ZeroDivisionError
                    Linv[i, k] = (ei[i] - sum([Linv[j, k] * L[i, j] for j in range(0, i)])) / L[i, i]
            return Linv
        except ZeroDivisionError:
            print('La matrice n\'est pas inversible')
            return numpy.zeros([n, n])
    except NonSquareMatrixError:
        print('La matrice n\'est pas carrée')
        return numpy.zeros([n, n])


# permet la factorisation LU d'une grande matrice bien plus rapidement (algorithme par blocs)
def factoLUBloc(M):
    n = M.shape[0]
    A = numpy.copy(M)
    try:
        if A.shape[0] != A.shape[1]:
            raise NonSquareMatrixError
        if n <= DIM_MAX:
            return factoLU(M)
        else:
            A11 = A[0:DIM_MAX, 0:DIM_MAX]
            A21 = A[DIM_MAX:n, 0:DIM_MAX]
            A12 = A[0:DIM_MAX, DIM_MAX:n]
            A22 = A[DIM_MAX:n, DIM_MAX:n]
            [L11, U11] = factoLU(A11)
            L21 = numpy.matmul(A21, inverserMatriceTriangulaireSuperieure(U11))
            U12 = numpy.matmul(inverserMatriceTriangulaireInferieure(L11), A12)
            B = A22 - numpy.matmul(L21, U12)
            [L22, U22] = factoLUBloc(B)

            # reconstruction de L
            L = numpy.zeros([n, n])
            L[0:L11.shape[0], 0:L11.shape[1]] = L11
            L[L11.shape[0]:n, 0:L11.shape[1]] = L21
            L[L11.shape[0]:n, L11.shape[1]:n] = L22

            # reconstruction de U
            U = numpy.zeros([n, n])
            U[0:U11.shape[0], 0:U11.shape[1]] = U11
            U[0:U11.shape[0], U11.shape[1]:n] = U12
            U[U11.shape[0]:n, U11.shape[1]:n] = U22
            return [L, U]
    except NonSquareMatrixError:
        print('La matrice n\'est pas carrée')
        return [numpy.zeros([n, n]), numpy.zeros([n, n])]


# permet une factorisation LU plus rapide sur les matrices symétriques
def factoGaussSymetrique(M):
    A = numpy.copy(M)
    n = A.shape[0]
    try:
        if A.shape[0] != A.shape[1]:
            raise NonSquareMatrixError
        L = numpy.identity(n)
        D = numpy.zeros([n, n])
        for j in range(0, n):
            D[j, j] = A[j, j] - sum([L[j, k] * L[j, k] * D[k, k] for k in range(0, j)])
            for i in range(j + 1, n):
                L[i, j] = (A[i, j] - sum([L[i, k] * L[j, k] * D[k, k] for k in range(0, j)])) / D[j, j]
        return [L, D]
    except NonSquareMatrixError:
        print('La matrice n\'est pas carrée')
        return [numpy.zeros([n, n]), numpy.zeros([n, n])]


def testPerturbation(M):
    A = numpy.copy(M)
    n = A.shape[0]
    x = numpy.ones(n)
    b = numpy.matmul(A, x)
    print('x =\n ', resoLU(A, b))

    print('Test perturbation sur b (+10^-8 à une composante)\n')

    Db = numpy.zeros(n)
    Db[0] = 10 ** (-8)
    x0 = resoLU(A, b + Db)
    print('x =\n ', x0)

    print('\n norme2 :')
    print('cond(A) = ', numpy.linalg.cond(A, 2))
    print('|Dx|/|x| = ', numpy.linalg.norm(x0 - x, 2) / numpy.linalg.norm(x, 2))
    print('cond(A)* |Db|/|b| = ', numpy.linalg.cond(A, 2) * (numpy.linalg.norm(Db, 2) / numpy.linalg.norm(b, 2)))

    print('\n norme1 :')
    print('cond(A) = ', numpy.linalg.cond(A, 1))
    print('|Dx|/|x| = ', numpy.linalg.norm(x0 - x, 1) / numpy.linalg.norm(x, 1))
    print('cond(A)* |Db|/|b| = ', numpy.linalg.cond(A, 1) * (numpy.linalg.norm(Db, 1) / numpy.linalg.norm(b, 1)))

    print('\n normeINF :')
    print('cond(A) = ', numpy.linalg.cond(A, numpy.inf))
    print('|Dx|/|x| = ', numpy.linalg.norm(x0 - x, numpy.inf) / numpy.linalg.norm(x, numpy.inf))
    print('cond(A)* |Db|/|b| = ',
    numpy.linalg.cond(A, numpy.inf) * (numpy.linalg.norm(Db, numpy.inf) / numpy.linalg.norm(b, numpy.inf)))

    print('Test perturbation sur A (+10^-8 à un élément)\n')
    Da = numpy.zeros([n, n])
    Da[0, 0] = 10 ** (-8)
    x1 = resoLU(A + Da, b)
    print('x =\n ', x0)

    print('\n norme2 :')
    print('cond(A) = ', numpy.linalg.cond(A, 2))
    print('|Dx|/|x + Dx| = ', numpy.linalg.norm(x1 - x, 2) / numpy.linalg.norm(x1, 2))
    print('cond(A)* |Da|/|a| = ', numpy.linalg.cond(A, 2) * (numpy.linalg.norm(Da, 2) / numpy.linalg.norm(A, 2)))

    print('\n norme1 :')
    print('cond(A) = ', numpy.linalg.cond(A, 1))
    print('|Dx|/|x + Dx| = ', numpy.linalg.norm(x1 - x, 1) / numpy.linalg.norm(x1, 1))
    print('cond(A)* |Da|/|a| = ', numpy.linalg.cond(A, 1) * (numpy.linalg.norm(Da, 1) / numpy.linalg.norm(A, 1)))

    print('\n norme2 :')
    print('cond(A) = ', numpy.linalg.cond(A, numpy.inf))
    print('|Dx|/|x + Dx| = ', numpy.linalg.norm(x1 - x, numpy.inf) / numpy.linalg.norm(x1, numpy.inf))
    print('cond(A)* |Da|/|a| = ', numpy.linalg.cond(A, numpy.inf) * (numpy.linalg.norm(Da, numpy.inf) / numpy.linalg.norm(A, numpy.inf)))

# main


# test factorisation par bloc + comparaison de temps
A = numpy.random.rand(400, 400)
start = time.time()
[L, U] = factoLU(A)
end = time.time()
print('Temps pour factorisation LU de matrice 400x400:', end - start)
print('Résultats de la factorisation  : ')
print('||A- LU||2 = \n', numpy.linalg.norm(A - numpy.matmul(L, U), 2))
print('||A- LU||1 = \n', numpy.linalg.norm(A - numpy.matmul(L, U), 1))

start = time.time()
[L, U] = factoLUBloc(A) 
end = time.time()
print('Temps pour factorisation LU par bloc de matrice 400x400:', end - start)
print('Résultats de la factorisation par bloc : ')
print('||A- LU||2 = \n', numpy.linalg.norm(A - numpy.matmul(L, U), 2))
print('||A- LU||1 = \n', numpy.linalg.norm(A - numpy.matmul(L, U), 1))


# test factorisation et resolution LU ainsi que la resolution en prenant en compte le profil
print('Factorisation de matrices creuses avec le profil')
A = numpy.array([[1., 1., 0., 0.], [0., 1., 1., 0.], [1., 1., 1., 0.], [0., 1., 1., 1.]])
profil = [[0, 1], [1, 2], [0, 2], [1, 3]]
b = numpy.array([2., 2., 3., 3.])
print('A = \n', A)
[L, U] = factoLU(A)
print('L = \n', L)
print('U = \n', U)
print('x =\n ', resoLU(A, b))
print('x(méthode profil) =\n ', profilLU(A, profil, b))

# Méthode du profil avec matrice bande
print('Méthode du profil avec matrice bande')
A = numpy.array([[2., 1., 0., 0.], [1., 3., 2., 0.], [0., 2., 4., 3.], [0., 0., 3., 5.]])
profil = [[0, 1], [0, 2], [1, 3], [2, 3]]
b = numpy.array([2., 2., 3., 3.])
print('x =\n ', resoLU(A, b))
print('x(méthode profil) =\n ', profilLU(A, profil, b))


# Matrices à grand conditionnement et test de perturbation
print('Test de perturbation avec la matrice de Wilson\n')
A = numpy.array([[10., 7., 8., 7.], [7., 5., 6., 5.], [8., 6., 10., 9.], [7., 5., 9., 10.]])  # matrice de Wilson
testPerturbation(A)
print('Test de perturbation avec la matrice de Hilbert à l\'ordre 8\n')
A = hilbert(8)
testPerturbation(A)


# test d'inversion de matrices triangulaires
print('\nTest d\'inversion de matrices triangulaires')
print(inverserMatriceTriangulaireSuperieure(numpy.array([[1., 2.], [0., 1.]])))
print(numpy.linalg.inv(numpy.array([[1., 2.], [0., 1.]])))
print(inverserMatriceTriangulaireInferieure(numpy.array([[3., 0., 0.], [1., 4., 0.], [1., 1., 1.]])))
print(numpy.linalg.inv(numpy.array([[3., 0., 0.], [1., 4., 0.], [1., 1., 1.]])))


# test de factorisation gauss-symétrique
print('\nTest de factorisation Gauss-symétrique')
A = numpy.array([[1, 3, 3], [3, 4, 8], [3, 8, 2]])
[L, D] = factoGaussSymetrique(A)
print('||A- LDLt||1 = \n', numpy.linalg.norm(A - numpy.matmul(numpy.matmul(L, D), numpy.transpose(L)), 1))
print('||A- LDLt||2 = \n', numpy.linalg.norm(A - numpy.matmul(numpy.matmul(L, D), numpy.transpose(L)), 2))
