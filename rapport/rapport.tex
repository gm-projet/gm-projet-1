\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[colorlinks = true,
			linkcolor = blue]{hyperref}
\usepackage{amsfonts, amsmath, amssymb, minted, graphicx}

%opening
\title{Projet d'analyse numérique : résolution de système linéaire par la méthode de Gauss}
\author{Robin \textsc{Langlois} et Nathan \textsc{Flambard}}

\begin{document}

\maketitle

\begin{abstract}
Projet de GM3 de la première vague, MMSN projet 09. L'idée est de résoudre des systèmes linéaires de la forme $Ax = b$ en utilisant la factorisation LU puis l'élimination de Gauss. On étudiera également le conditionnement et les optimisations possibles sur différents cas particuliers.
\end{abstract}

\newpage

\tableofcontents

\newpage

\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

Dans bien des domaines, notamment en modélisation mathématique, en optimisation ou bien en physique, nous sommes amenés à résoudre des systèmes linéaires du type $Ax = b$, où $A$ est une matrice carrée de $M_n(\mathbb{K})$ et $x$ et $b$ sont des vecteurs de $\mathbb{K}^n$. Puisque cette résolution est fortement utilisée, il est donc primordial de l'implémenter en machine lorsque la dimension du système devient très grande (peut aller jusqu'à des centaines de milliers voire des millions d'équations). De prime abord, et si l'on a pratiqué un peu d'analyse matricielle, on pourrait se dire que la solution de ce système est très simple, et que ce n'est que $x = A^{-1}b$ (si $A$ est inversible, bien sûr). Cependant, l'inversion de matrice, notamment pour une matrice de très grande dimension, est une opération extrêmement coûteuse. On cherche donc à \textbf{optimiser} ce processus, de manière à le rendre très rapide même pour un système de grande dimension comme l'on en utilise de plus en plus, par exemple dans le cadre du Big Data.

La méthode que nous allons voir pour cela est la méthode d'élimination de Gauss : elle utilise notamment la factorisation LU, qui consiste à factoriser sous certaines conditions une matrice en deux matrices triangulaires : une inférieure et une supérieure. Une fois cette factorisation obtenue, nous verrons qu'il est facile d'en déduire la solution du système linéaire. Le principe de l'élimination de Gauss est exactement celui que l'on applique en résolvant des petits systèmes linéaires, où l'on cherche à éliminer des inconnues dans les équations grâce à un pivot. Il s'agit simplement ``d'automatiser" cette méthode et de l'exprimer matriciellement.

Enfin, après avoir mis en évidence l'enjeu du conditionnement, nous dévelop\-perons les optimisations supplémentaires possibles pour certaines matrices structurées, notamment les matrices pleines, creuses, ou symétriques.

\newpage

\section{Présentation du problème et de sa résolution mathématique et numérique}

\subsection{Problème et résolution mathématique}

On a donc vu que le problème initial est de trouver une méthode alternative pour résoudre $Ax = b$.

\subsubsection{La factorisation LU}

On cherche donc la factorisation telle que $A = LU$ avec $L$ une matrice triangulaire inférieure à diagonale unité et $U$ une matrice triangulaire supérieure. Nous avons vu en cours d'analyse numérique qu'une matrice $A$ inversible admet une décomposition LU si et seulement si toutes les sous-matrices de $A$ sont inversibles, ou, de manière équivalente, si et seulement si tous les mineurs principaux de $A$ sont non nuls. Montrons cette condition :

On souhaite déterminer les coefficients $L_{i\,j}$ et $U_{i\,j}$ tels que :
$$A_{l\,c} = (LU)_{l\,c} = \sum_{k=1}^n L_{l\,k}U_{k\,c} = \sum_{k=1}^{min(l;c)} L_{l\,k}U_{k\,c}$$
$\forall l \in \{1;...;n\}$ et $\forall c \in \{1;...;n\}$

A l'étape 1, pour $l = 1$ on a $A_{1\,c} = L_{1\,1}U_{1\,c} = U_{1\,c}$ car $L$ est à diagonale unité. On déduit ainsi la première ligne de $A$, et on a en particulier $U_{1\,1} = A_{1\,1}$. En prenant $c = 1$, on obtient $A_{l\,1} = L_{l\,1}U_{1\,1}$ où $U_{1\,1}$ est connu, ce qui nous donne
$$L_{l\,1} = \frac{A_{l\,1}}{U_{1\,1}}$$
ce qui impose donc à $U_{1\,1}$ d'être non nul, et par conséquent à $A_{1\,1}$. La sous-matrice d'ordre 1 $A_{1\,1}$ doit donc être inversible.

A l'étape p, on suppose connues les $p - 1$ premières lignes de $U$ et les $p - 1$ premières colonnes de $L$. Par construction, on a $L_p U_p = A_p$, soit :


$$
\begin{pmatrix}
1           & 0      & \ldots       & 0      \\
L_{2\,1}    & \ddots & \ddots       & \vdots \\
\vdots      & \ddots & \ddots       & 0      \\
L_{p\,1}    & \ldots & L_{p\,(p-1)} & 1
\end{pmatrix}
\times
\begin{pmatrix}
U_{1\,1}    & \ldots & \ldots   & U_{1\,p} \\
0           & \ddots &          & \vdots \\
\vdots      &        & \ddots   & \vdots \\
0           & \ldots & 0        & U_{p\,p}
\end{pmatrix}
=
\begin{pmatrix}
A_{1\,1}    & \ldots & \ldots   & A_{1\,p} \\
\vdots      &        &          & \vdots \\
\vdots      &        &          & \vdots \\
A{p\,1}     & \ldots & \ldots   & A_{p\,p}
\end{pmatrix}
$$

On a donc la relation $det(L_p)det(U_p) = det(A_p)$. Or, $L_p$ est triangulaire à diagonale unité, on a alors $det(L_p) = 1$ d'où $det(U_p) = det(A_p)$.

D'autre part, on sait par la relation définie précédemment que, si on pose $c = p$, alors $\forall l > p$ :
$$A_{l\,p} = \sum_{k=1}^p L_{l\,k}U_{k\,p} \Leftrightarrow A_{l\,p} = L_{l\,p}U_{p\,p} + \sum_{k=1}^{p-1} L_{l\,k}U_{k\,p} 
\Leftrightarrow L_{l\,p} = \frac{A_{l\,p} - \sum_{k=1}^p L_{l\,k}U_{k\,p}}{U_{p\,p}}$$
ce qui impose donc encore à $U_{p\,p}$ d'être non nul. Comme cela est valable pour tout $p$, alors tous les termes diagonaux de $U$ doivent être non nuls, donc le déterminant de $U_p$ $\forall p \in \{1;...;n\}$ sera non nul, et il en sera donc de même pour $A_p$. On obtient donc que toutes les sous-matrices $A_p$ de $A$ doivent être inversibles.


Il serait maintenant intéressant de voir le lien que cela peut avoir avec les pivots. En effet, cette condition entraîne le fait que tous les pivots sont non nuls. Prenons les équations qu'utilisent la méthode d'élimination de Gauss, que l'on peut retrouver dans le cours d'analyse numérique :
$$L_{i\,k}^{[k+1]} = A_{i\,k}^{[k]} / A_{k\,k}^{[k]}  \; \; \forall i \geqslant k + 1$$
$$A_{i\,j}^{[k+1]} = A_{i\,j}^{[k]} - L_{i\,k}^{[k+1]}\times A_{k\,j}^{[k]} \; \; \forall j \geqslant k  + 1$$
$$b_{i}^{[k+1]} = b_{i}^{[k]} - L_{i\,k}^{[k+1]} \times b_{k}^{[k]} \; \; \forall i \geqslant k + 1$$

On observe que dans ces équations, $\forall k \in \{1;...;n\}$, $A_{k\,k}$ n'est plus modifié à partir de l'étape $k$ et donc que cette valeur restera inchangée à la fin de l'algorithme. C'est également à cette étape uniquement qu'on la divise à $A_{i\,k}$ dans la première équation. Cependant, à la fin, la matrice $A$ contient en fait la matrice $U$ dans sa partie supérieure (diagonale comprise). La diagonale de $A$ est donc modifiée lors du programme pour devenir celle de $U$. Comme le déterminant de $U$ est aussi celui de $A$, et que le déterminant de $A$ est non nul par hypothèse, alors tous les coefficients diagonaux de $U$ (et donc les $A_{k\,k}$ définis à chaque étape) sont non nuls, et ce sont aussi les pivots.

\subsubsection{Résolution du système}

Une fois la factorisation LU obtenue, la chose intéressante (et qui est le but premier de cette manoeuvre) est bien entendu de pouvoir résoudre simplement le système $Ax = b$. Puisque $A = LU$, on a donc $LUx = b$. La méthode vue en cours consiste à poser et à résoudre le système $Ux = y$, ce qui nous donne en remplaçant $Ly = b$, qu'on résout également. Dans la pratique, on résoudra $Ly = b$ en premier car nos données sont $L$, $U$ et $b$. Ces deux problèmes sont simples à résoudre car nos matrices sont triangulaires, ce qui veut dire que nous pouvons résoudre le système ``séquentiellement'' : on a une équation à une inconnue, que l'on peut résoudre immédiatement, puis une équation à deux inconnues, dans laquelle on peut injecter l'inconnue qu'on vient de trouver, etc. Cet algorithme est nommé la descente remontée. Finalement, la résolution d'un tel système est relativement peu coûteuse : on peut montrer que la complexité est en $O(n^2)$, ce qui reste relativement acceptable pour une telle opération.

Cependant, nous allons voir qu'avec la méthode d'élimination de Gauss, il est possible de simplifier la méthode et de ne résoudre qu'un seul système triangulaire. En effet, en modifiant $b$ à chaque étape de l'algorithme, on peut se ramener à un simple système $Ux = \tilde{b}$, ce qui ne nécessite donc qu'un algorithme de montée. Il suffit simplement, à chaque fois qu'on parcourt la ligne i de la matrice $A$ transformée, d'effectuer les mêmes opérations sur $b_i$ que sur $A_{i\,j}$. Cela nous permet donc, lorsqu'on désire simplement résoudre le système $Ax = b$ sans connaître la factorisation $LU$, de nous affranchir totalement de l'utilisation de la matrice $L$.

\subsubsection{Le conditionnement}

Le conditionnement est une notion vaste dans l'analyse matricielle, et clairement fondamentale en analyse numérique. En effet, pour une matrice carrée $A$, le conditionnement de A (noté $cond_q (A)$) subordonné à la norme q est égal à $\left\|A\right\|_q \times \left\|A^{-1}\right\|_q$. Ce nombre est une mesure de la robustesse des systèmes linéaires $Ax = b$ que l'on peut définir avec notre matrice $A$. En effet, plus le conditionnement de A est grand, plus une légère perturbation sur les coefficients de A aura une influence importante sur la variation des coefficients de la solution $x$ : pour une matrice mal conditionnée (i.e à grand conditionnement), une différence presque imperceptible sur les valeurs de $A$ pourra être répercutée par exemple 100 ou 1000 fois plus forte sur les valeurs de $x$. Cela soulève plusieurs problèmes : tout d'abord, lorsqu'on cherche à faire des mesures en physique, ou qu'on cherche à estimer un paramètre de population en statistiques, nous ne disposons jamais de la valeur exacte de la grandeur que l'on cherche à mesurer : nous n'en avons qu'une approximation, limitée par les appareils de mesure ou matériellement par la taille de notre échantillon. Mais quand bien même nos appareils seraient idéaux, ou nous aurions disons de la chance, et que nous obtenions la ``vraie'' valeur du paramètre (même si son existence pourrait prêter à un plus large débat), nous serions tout de même incapables de la représenter de manière exacte numériquement du fait du codage des réels en machine. Il apparaît donc tout à fait que les erreurs sur nos coefficients s'accumulent rapidement, et qu'il est donc vital de garder à l'esprit cette idée de conditionnement.

Si l'on considère que la matrice $A$ n'a pas ses coefficients exacts, on peut modéliser cela par une perturbation $\delta_A$ telle que $A' = A + \delta_A$ où $A'$ serait donc la matrice utilisée lors de la résolution du système. La solution du système $A'x' = b$ deviendrait donc une solution également perturbée $x' = x + \delta_x$. On peut alors montrer que :
$$ \frac{\left\|\delta_x\right\|}{\left\|x +\delta_x\right\|} \leqslant 
cond(A) \frac{\left\|\delta_A\right\|}{\left\|A\right\|}$$

Si, au contraire, on considère que c'est le second membre qui a été perturbé, on posera donc $b' = b + \delta_b$ et on cherchera à résoudre $Ax' = b'$ où, de la même manière, la solution a été perturbée. Dans ce cas, on aura :
$$ \frac{\left\|\delta_x\right\|}{\left\|x\right\|} \leqslant 
cond(A) \frac{\left\|\delta_b\right\|}{\left\|b\right\|}$$

On peut montrer aussi que ces deux inégalités sont optimales : c'est-à-dire qu'on trouvera toujours des cas (avec $b$ et la perturbation $\delta_A$ ou $\delta_b$ non nuls) où l'égalité sera satisfaite.


\subsection{Résolution numérique}

\subsubsection{Élimination de Gauss}
La methode d'élimination consiste à transformer le système $Ax = b $ (noté $A^{[1]} x = b^{[1]} $) en un système équivalent $A^{[n]} x = b^{[n]} $ où $A^{[n]}$ est une matrice triangulaire supérieure. Pour implémenter cette methode, on utilise les relations suivantes pour passer du système $A^{[k]} x = b^{[k]}$ au système $A^{[k+1]}x = b^{[k+1]} $ :
$$L_{i\,k}^{[k+1]} = A_{i\,k}^{[k]} / A_{k\,k}^{[k]}  \; \; \forall i \geqslant k + 1$$
$$A_{i\,j}^{[k+1]} = A_{i\,j}^{[k]} - L_{i\,k}^{[k+1]}\times A_{k\,j}^{[k]} \; \; \forall j \geqslant k  + 1$$
$$b_{i}^{[k+1]} = b_{i}^{[k]} - L_{i\,k}^{[k+1]} \times b_{k}^{[k]} \; \; \forall i \geqslant k + 1$$
On remarque qu'il n'est pas nécessaire de stocker toute la matrice $L^{[k+1]}$ mais seulement les valeurs des $L_{i\,k}^{[k+1]} $ avec $i \geqslant k+1 $ que l'on pourra stocker dans une seule variable (appelée $p$ dans le programme) et les relations deviennent : 
$$p_{i}^{[k+1]} = A_{i\,k}^{[k]} / A_{k\,k}^{[k]}  \; \; \forall i \geqslant k + 1$$
$$A_{i\,j}^{[k+1]} = A_{i\,j}^{[k]} - p_{i}^{[k+1]}\times A_{k\,j}^{[k]} \; \; \forall j \geqslant k  + 1$$
$$b_{i}^{[k+1]} = b_{i}^{[k]} - p_{i}^{[k+1]} \times b_{k}^{[k]} \; \; \forall i \geqslant k + 1$$
Une fois le système $A^{[n]} x = b^{[n]} $ obtenu, on peut le résoudre facilement par remontée avec les relations suivantes :
$$ x_{i} = (b_{i}^{[n]} - \sum_{j=i+1}^{n} A_{i\,j}^{[n]})/A_{i\,i}^{[n]}$$
La complexité est de $O(n^{3})$ pour la détermination du système $A^{[n]} x = b^{[n]} $ et de $O({n^{2}})$ 

\subsubsection{Matrices creuses}
Une matrice creuse est une matrice dont une ou plusieurs lignes commencent ou finissent par un ou plusieurs coefficients nuls, en particulier les matrices bandes sont des matrices creuses. On appellera profil de la matrice $A$ les vecteurs $P^{-}(A)$ et $P^{+}(A)$ dont les composantes $P^{-}_{i}(A)$ et $P^{+}_{i}(A)$ correspondent aux indices du premier coefficient non nul et du dernier coefficient non nul de la ligne $i$ de $A$. On peut montrer que la factorisation $LU$ conserve le profil. C'est à dire que pour $A = LU$ on aura $P^{-}(A) = P^{-}(L) $ et $P^{+}(A) = P^{+}(U)$. Cela nous permet de réduire le nombre d'itérations nécessaires à la résolution du système linéaire $Ax =b$ quand on a connaissance du profil de $A$. Les relations pour implémenter la méthode de Gauss deviennent alors :
$$p_{i}^{[k+1]} = A_{i\,k}^{[k]} / A_{k\,k}^{[k]}  \; \; \forall i \geqslant k + 1$$
$$A_{i\,j}^{[k+1]} = A_{i\,j}^{[k]} - p_{i}^{[k+1]}\times A_{k\,j}^{[k]} \; \; \forall j \; ; \; \max (\; k+1\;,\;P^{-}_{i}(A)\;) \leqslant j \leqslant \ P^{+}_{i}(A)$$
$$b_{i}^{[k+1]} = b_{i}^{[k]} - p_{i}^{[k+1]} \times b_{k}^{[k]} \; \; \forall i \geqslant k + 1$$
De même lors de la résolution du système $A^{[n]} x = b^{[n]} $ , on peut encore réduire le nombre d'itérations en utilisant les relations suivantes :
$$ x_{i} = (b_{i}^{[n]} - \sum_{j=i+1}^{P^{+}_{i}(A)} A_{i\,j}^{[n]})/A_{i\,i}^{[n]}$$


\subsubsection{Méthode par blocs}
La méthode de factorisation par blocs permet d' obtenir la factorisation $LU$ d'une matrice $A$ de grande taille plus rapidement qu'avec la méthode de Gauss. Pour mettre en place cette méthode, on commence par diviser $A$ en quatre sous-matrices :
$$A = \begin{pmatrix}
A_{1\,1} & A_{1\,2}\\
A_{2\,1} & A_{2\,2}
\end{pmatrix}$$
Où $A_{1\, 1}$ et $A_{2\,2}$ sont des matrices carrées de taille $m$ et $n - m$ (avec $A$ de taille $n \times n$). On décompose $L$ et $U$ de la même façon :
$$A = \begin{pmatrix}
L_{1\,1} & 0\\
L_{2\,1} & L_{2\,2}
\end{pmatrix}
\quad
U = \begin{pmatrix}
U_{1\,1} & U_{1\,2}\\
0 & U_{2\,2}
\end{pmatrix}$$
$A = LU$ donne donc :
$$A_{1\,1} = L_{1\,1} U_{1\,1}$$
$$A_{2\,1} = L_{2\,1} U_{1\,1}$$
$$A_{1\,2} = L_{1\,1} U_{1\,2}$$
$$A_{2\,2} = L_{2\,1} U_{1\,2} + L_{2\,2}U_{2\,2}$$
Pour trouver $L$ et $U$ on procède de la manière suivante : on utilise la méthode de Gauss pour factoriser $A_{1\,1}$ et ainsi obtenir  $L_{1\,1}$ et $U_{1\,1}$. On calcule ensuite  $U_{1\,2} =L_{1\,1}^{-1} A_{1\,2} $ et $L_{2\,1} = A_{2\,1} U_{1\,1}^{-1}$. Enfin on pose $B = A_{2\,2}-L_{2\,1} U_{1\,2} = L_{2\,2}U_{2\,2} $ et on effectue la factorisation par bloc (appel récursif)  de $B$ pour obtenir $L_{2\,2}$ et $U_{2\,2}$.
Pour calculer $L_{1\,1}^{-1}$ et $U_{1\,1}^{-1}$, on utilise le fait qu'inverser une matrice revient à résoudre les systèmes $Ax_{i} = e_{i}$ où les $e_{i}$ sont les vecteurs de la base canonique et on aura alors $A_{i\; j}^{-1} = (x_{j})_{i}$. Or comme $ L_{1\,1}$ et $ U_{1\,1}$ sont des matrices triangulaires les systèmes précédents peuvent se résoudre par descente et remontée. La méthode par blocs est bien plus rapide que la factorisation $LU$ par méthode de Gauss sur les grandes matrices car on factorise seulement les sous-matrices de taille $m$ sur la diagonale de $A$ puis on déduit le reste des coefficients de $L$ et $U$ par descente remontée sur des matrices de taille $m$ et produits matriciels.

La complexité de cet algorithme va dépendre de $n$ mais également de $m$. En effet on réalise $\frac{n}{m}$ appels récursifs et à chaque appel on fait deux inversions de matrices triangulaires de taille $m\times m$ (complexité $O(m^{3})$) et deux multiplications de matrices de taille $m\times m $ et $m \times (n-m)$ ou inférieure (complexité $O(m^{2}(n-m))$). On aura donc une complexité totale de $n^{2}m^{4}- nm^{5} $. On constate donc que le temps de calcul dépend de la valeur de $m$ mais qu'il n'est pas intéressant de la maximiser ou de la minimiser (ce qui est logique car si l'on procède ainsi on retombe sur la méthode de Gauss).
\subsubsection{Matrices symétriques}
Pour une matrice symétrique on a $A = A^{T}$ ce qui après factorisation $LU$ donne $LU = U^{T}L^{T}$. Cependant on n'aura pas $U^{T}= L$ car $L$ est à diagonale unité, ce qui en général n'est pas vrai pour $U$. Donc on ne peut pas optimiser l'algorithme de la factorisation en posant $U^{T}= L$. On peut montrer cependant que si $A$ est symétrique alors on peut la décomposer sous la forme $A = LDL^{T}$ où $L$ est triangulaire à diagonale unité et $D$ une matrice diagonale (on aura en fait $U = DL^{T}$). Cette factorisation est également appelée la factorisation de Crout. On aura donc :
$$ A_{i\, j} = \sum_{k=1}^{j} L_{i\, k}L_{j\, k}D_{k\,k} \; \forall i \geqslant j$$
ce qui donne pour $i = j$ en utilisant le fait que $L$ est à diagonale unité ($L_{i\,i} = 1 \; \forall i$):
$$ A_{i\,i} = D_{i\,i} + \sum_{k=1}^{i-1} (L_{i\, k})^{2}D_{k\,k} $$
$$\Rightarrow  D_{i\,i} = A_{i\,i} - \sum_{k=1}^{i-1} (L_{i\, k})^{2}D_{k\,k} \; \forall i = j$$
de même pour $i > j $ :
$$ A_{i\, j} = L_{i\,j}D_{j\,j} + \sum_{k=1}^{j-1} L_{i\, k}L_{j\, k}D_{k\,k}$$
$$\Rightarrow L_{i\,j} = (A_{i\,j} - \sum_{k=1}^{j-1} L_{i\, k}L_{j\, k}D_{k\,k} )/ D_{j\,j} \; \forall i > j$$
Une fois la décomposition obtenue, elle permet de résoudre un système $Ax=b$ en résolvant $Ly = b$ puis $L^{T}x = D^{-1}y$ (on note que le calcul de $D^{-1}$ est très peu coûteux car $D$ est diagonale). Cette méthode nécessite deux fois moins de calcul que la méthode de Gauss.

\section{Résultats informatiques}
\subsection{Résolution de systèmes à grand conditionnement}
\subsubsection{Matrice de Wilson}
La matrice de Wilson est la matrice définie par :
$$A = 
\begin{pmatrix}
10 & 7 & 8  & 7 \\
7  & 5 & 6  & 5 \\
8  & 6 & 10 & 9 \\
7  & 5 & 9  & 10
\end{pmatrix}$$
Cette matrice possède un fort conditionnement (environ $3000$ pour la norme $2$) et peut être représentée de manière exacte avec les flottants car tout les coefficients sont des entiers assez petits. Afin d'étudier les inégalités sur le conditionnement vues précédemment, on pose $b$ afin que la résolution du système $Ax=b$ donne comme solution :
$$x = 
\begin {pmatrix}
 1 \\
 1 \\
 1 \\
 1 
\end {pmatrix}
$$
Puis on compare les deux parties de l'inégalité dans le cas d'une perturbation sur $b$ et dans le cas d'une perturbation sur $A$. On obtient alors :

\includegraphics[scale=0.6]{images/CondWilson.png}

On remarque que les deux côtés de l'inégalité sont à chaque fois assez proches (rapport $1/10$ au maximum).  





\subsubsection{Matrices de Hilbert}
Les matrices de Hilbert sont des matrices carrées définies par :
$$A_{i\; j} = \frac{1}{i+j-1} \; \forall i \; \forall j $$
Ces matrices possèdent également un grand conditionnement et ne sont pas représentables exactement avec des flottants (présence de coefficients $\frac{1}{3}$ par exemple). Pour notre projet, nous avons utilisé la matrice de Hilbert d'ordre $8$ (conditionnement d'environ $15\times 10^{9} $ pour la norme $2$). Comme pour la matrice de Wilson, on choisit $b$ de manière à avoir $Ax=b \Rightarrow x_{i} = 1 \; \forall i$ et on étudie les inégalités pour une perturbation sur $b$ et sur $A$. les résultats sont les suivants :

\includegraphics[scale=0.6]{images/CondHilbert8.png}

On constate cette fois que les inégalités sont beaucoup moins précises que pour la matrice de Wilson.  

\subsection{Efficacité de la factorisation par bloc}
On a vu précédemment que la factorisation par bloc est une méthode plus efficace que la méthode de Gauss pour les grandes matrices. Nous avons donc comparé les temps d'exécution des deux méthodes pour des matrices de taille $400\times400$ et $1000\times1000$ et nous avons obtenu les résultats suivants :

\includegraphics[scale=0.6]{images/factovsbloc400.png}

\includegraphics[scale=0.6]{images/factovsbloc1000.png}

Les temps obtenus nous montrent que la méthode par bloc est bien plus rapide que la méthode de Gauss. Cependant, on remarque que la méthode est moins précise que la méthode de Gauss.    

\newpage
\section*{Conclusion}
\addcontentsline{toc}{section}{Conclusion}

Lors de ce projet, nous avons vu comment effectuer la factorisation LU d'une matrice et la résolution du système linéaire $Ax = b$. Nous avons vu les conditions mathématiques liées à tout cela, et nous avons également vu les optimisations possibles en fonction de la structure de la matrice entrée. Enfin, après avoir mis en évidence le caractère fondamental du conditionnement, nous avons travaillé sur deux exemples de matrices à fort conditionnement. En plus de réviser et d'appliquer le cours d'analyse numérique, nous avons pendant ce projet appris à utiliser le langage Python, qui est très adapté à ce genre d'utilisation. Pour faciliter la gestion de ce projet et la collaboration sur celui-ci, nous avons également utilisé le service Git, qui est extrêmement pratique pour travailler à plusieurs et assurer la sauvegarde du projet. De plus, nous sommes amenés à l'utiliser de plus en plus, notamment dans les projets futurs et la vie professionnelle.

La nature même de ce projet venait d'un désir d'optimisation. Aujourd'hui, dans de nombreux domaines, l'optimisation est fondamentale. Là où les machines n'évoluent plus aussi vite qu'avant, les programmes que nous utilisons sont de plus en plus gourmands en ressources. Il est donc primordial d'optimiser nos programmes afin de les rendre utilisables par tous. Cela est d'autant plus important lorsqu'on travaille avec des données de grande dimension, comme en data science ou même dans le jeu vidéo où les environnements sont de plus en plus détaillés. Enfin, nous conclurons sur cette citation de Mireille \textsc{Sitbon}, légèrement humoristique mais non moins philosophique : ``L'informatique, ça fait gagner beaucoup de temps... à condition d'en avoir beaucoup devant soi !'' 

\newpage
\section*{Bibliographie}
\addcontentsline{toc}{section}{Bibliographie}

\renewcommand\refname{}
\begin{thebibliography}{5}
\bibitem{analNumLascauxTheodor}
    Patrick \textsc{Lascaux} et Raymond \textsc{Théodor},
    \emph{Analyse numérique matricielle appliquée à l'art de l'ingénieur : 1. Méthodes directes}.
    Dunod,
    2000.
	  
\bibitem{brezinski}
    Claude \textsc{Brezinski} et Michela \textsc{Redivo-Zaglia},
    \emph{Méthodes numériques directes : algèbre linéaire et non linéaire}.
    Ellipses,
    2006.
    
\bibitem{methodeParBlocs}
    Jocelyne \textsc{Erhel}, Nabil \textsc{Nassif} et Bernard \textsc{Philippe},
    \emph{Calcul matriciel et systèmes linéaires}. \\
    \url{https://www.irisa.fr/sage/jocelyne/cours/INSA/direct.pdf} \\
    2012.
    
\bibitem{profil}
    Eric \textsc{Lunéville},
    \emph{Résolution des systèmes linéaires}. \\
    \url{http://perso.ensta-paristech.fr/~lunevill/MA261/MA261_cours3_col.pdf}\\
    2007.
    
\bibitem{analNum}
    Cours de Antoine \textsc{Tonnoir} à l'INSA de Rouen,
    \emph{Analyse numérique : méthodes directes pour résoudre des systèmes linéaires}.\\
    2018.
\end{thebibliography}


\newpage
\section*{Annexes}
\addcontentsline{toc}{section}{Annexes}

Pour implémenter notre programme, nous avons choisi Python, pour sa simplicité d'utilisation ainsi que pour le large choix de bibliothèques numériques, notamment Scipy et Numpy. Cela nous a permis de pouvoir vérifier simplement nos résultats et de travailler sur des valeurs telles que le conditionnement sans pour autant devoir recoder la fonction. Les principales difficultés que nous avons rencontrées sont le fait que les indices commencent à 0, et que les \mintinline{python}{range} sont construites d'une manière particulière : en effet, pour une boucle \mintinline{python}{for i in range(0, 10)}, le premier argument de range est compris, et le deuxième n'est pas compris. Pour cette boucle, \mintinline{python}{i} prendra donc ses valeurs de 0 à 9. Il a donc fallu réécrire les algorithmes en prenant en compte cela et en réajustant tous les indices.

De plus, en Python, les arguments d'entrée d'une fonction (notamment les tableaux) sont passés par référence. Cela veut dire que si l'on modifie la valeur de la matrice entrée, alors elle sera modifiée à la fin de la fonction. Nous ne connaissions pas cette subtilité du langage Python et nous avons mis un certain temps à nous en rendre compte lors des premiers débuggages de notre programme. Nous avons réglé ce problème en copiant les entrées dans des nouvelles variables au début des fonctions.
Voici nos fonctions, suivies si nécessaires des cas de tests utilisés. Elles se situent toutes dans le fichier main.py.

\subsection{Factorisation LU}

\inputminted[firstline = 16, lastline = 40, fontsize = \footnotesize]{python}{../src/main.py}

\subsection{Résolution de $Ax = b$}

\inputminted[firstline = 43, lastline = 70, fontsize = \footnotesize]{python}{../src/main.py}

\subsection{Factorisation de $Ax = b$ pour une matrice creuse avec le profil}

\subsubsection{Fonction}

\inputminted[firstline = 73, lastline = 102, fontsize = \footnotesize]{python}{../src/main.py}

\subsubsection{Tests}

\inputminted[firstline = 286, lastline = 304, fontsize = \footnotesize]{python}{../src/main.py}

\subsection{Factorisation par blocs}

\subsubsection{Fonctions intermédiaires (inversions de matrices triangulaires)}

\inputminted[firstline = 105, lastline = 154, fontsize = \footnotesize]{python}{../src/main.py}

\subsubsection{Fonction pour la factorisation}

\inputminted[firstline = 157, lastline = 191, fontsize = \footnotesize]{python}{../src/main.py}

\subsubsection{Tests}

\inputminted[firstline = 316, lastline = 321, fontsize = \footnotesize]{python}{../src/main.py}

\inputminted[firstline = 267, lastline = 275, fontsize = \footnotesize]{python}{../src/main.py}

\subsection{Factorisation Gauss-symétrique}

\subsubsection{Fonction}

\inputminted[firstline = 194, lastline = 210, fontsize = \footnotesize]{python}{../src/main.py}

\subsubsection{Tests}

\inputminted[firstline = 324, lastline = 329, fontsize = \footnotesize]{python}{../src/main.py}


\subsection{Evaluation des deux membres de l'inégalité sur le conditionnement}

Puisque les tests pour les matrices à grand conditionnement étaient assez longs et répétitifs, nous les avons regroupés dans une fonction de test.

\subsubsection{Fonction}

\inputminted[firstline = 213, lastline = 262, fontsize = \footnotesize, breaklines]{python}{../src/main.py}

\subsubsection{Tests}

\inputminted[firstline = 307, lastline = 313, fontsize = \footnotesize, breaklines]{python}{../src/main.py}

\end{document}
